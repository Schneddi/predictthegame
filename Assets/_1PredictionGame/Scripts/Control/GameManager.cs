using System;
using UnityEngine;

namespace _1PredictionGame.Scripts.Control
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance;
        
        public static Camera MainCamera;
        [SerializeField] private NetworkingControl networkingControl;
        [SerializeField] private Camera mainCamera;

        private void Awake()
        {
            MainCamera = mainCamera;
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            _instance = this;
        }
    }
}
