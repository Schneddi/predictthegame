using System;
using System.Collections.Generic;
using _1PredictionGame.Scripts.Control;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Transporting;
using GameKit.Dependencies.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _1PredictionGame.Scripts.Player
{
    public class PlayerInputControl : NetworkBehaviour, PlayerInput.IPlayerObjectActions
    {
        [Header("Movement")]
        [Tooltip("The MoveSpeed will change, how fast the player accelerates.")]
        [SerializeField] private float moveSpeed = 500;
        [Tooltip("How fast the player decelerates.")]
        [SerializeField] private float slowDownSpeed = 5;
        [Tooltip("The MaxMoveSpeed will cap the maximum velocity, a player can have.")]
        [SerializeField] private float maxMoveSpeed = 8;
        [Tooltip("The force applied in the upwards direction,when pressing the jump key.")]
        [SerializeField] private float jumpForce = 1000;
    
        [Header("Mouse")]
        [Tooltip("How fast the character turns and looks with mouse movement.")]
        [SerializeField] private float turnRate = 1;
        [Tooltip("Maximum upwards look angle.")]
        [SerializeField] private float lookUpMax = 1;
        [Tooltip("Maximum downwards look angle.")]
        [SerializeField] private float lookDownMax = 1;
    
        [Header("GroundCheck")]
        [Tooltip("The height, at which ground is still detected.")]
        [SerializeField]private float groundDetectionLength = 0.025f;
        [Tooltip("Determines, which layers will be detected as ground.")]
        [SerializeField]private LayerMask groundLayers;
        [Tooltip("The width of the ground check, relative to the capsule collider max radius.")]
        [SerializeField][Range(0, 1)] private float groundCheckWidth = 0.95f;
    
        public PredictionRigidbody PredictionRigidbody;
    
        private PlayerInput _playerInput;
        private Rigidbody _rigidbody;
        private bool _isGrounded, _jumpTriggered;
    
        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _playerInput = new PlayerInput();
            _playerInput.PlayerObject.SetCallbacks(this);
            _playerInput.PlayerObject.Enable();
        
            PredictionRigidbody = ObjectCaches<PredictionRigidbody>.Retrieve();
            PredictionRigidbody.Initialize(GetComponent<Rigidbody>());
        }

        private void FixedUpdate()
        {
            GroundCheck();
        }

        public override void OnStartNetwork()
        {
            TimeManager.OnTick += TimeManager_OnTick;
            TimeManager.OnPostTick += TimeManager_OnPostTick;
        }

        public override void OnStopNetwork()
        {
            TimeManager.OnTick -= TimeManager_OnTick;
            TimeManager.OnPostTick -= TimeManager_OnPostTick;
        }

        private void TimeManager_OnTick()
        {
            if (IsOwner)
            {
                CreateReplicateData(out ReplicateData md);
                Move(md);
            }
        }
    
        private void TimeManager_OnPostTick()
        {
            CreateReconcile();
        }
    
        public override void CreateReconcile()
        {
            //We must send back the state of the rigidbody. 
            //More advanced states may require other values to be sent;
            ReconcileData rd = new ReconcileData(PredictionRigidbody);
            //Like with the replicate you could specify a channel here, though
            //it's unlikely you ever would with a reconcile.
            ReconcileState(rd);
        }
    
        [Reconcile]
        private void ReconcileState(ReconcileData data, Channel channel = Channel.Unreliable)
        {
            //Call reconcile on PredictionRigidbody passing in the values from data.
            PredictionRigidbody.Reconcile(data.PredictionRigidbody);
        }
    
        private void CreateReplicateData(out ReplicateData replicateData)
        {
            replicateData = default;
            if (!IsOwner)
                return;
        
            replicateData.Jump = _jumpTriggered;

            //Unset queued values.
            replicateData.MoveVector = _playerInput.PlayerObject.Move.ReadValue<Vector2>();
        
            _jumpTriggered = false;
        }
    
        [Replicate]
        private void Move(ReplicateData replicateData, ReplicateState state = ReplicateState.Invalid, Channel channel = Channel.Unreliable)
        {
            //If jumping move the character up
            if (replicateData.Jump && _isGrounded)
            {
                Vector3 jmpFrc = new Vector3(0f, jumpForce, 0f);
                PredictionRigidbody.AddForce(jmpFrc, ForceMode.Impulse);
            }
        
            //Add gravity to make the object fall faster. This is of course
            PredictionRigidbody.AddForce(Physics.gravity * 3f);

            Vector2 moveInput = replicateData.MoveVector;
            bool movePressed = moveInput.magnitude > 0;
        
            if(movePressed) MoveCharacter(moveInput);
            else SlowDownCharacter();
        
            PredictionRigidbody.Simulate();
        
        
            void SlowDownCharacter()
            {
                //map character speed to 2D
                Vector3 mappedRigidBodyForce = new Vector3(_rigidbody.velocity.x, 0, _rigidbody.velocity.y);
            
                // Create a deacceleration force opposite to the current motion
                //Vector3 decelerationForce = -mappedRigidBodyForce * (slowDownSpeed * (float)TimeManager.TickDelta);
                Vector3 decelerationForce = -mappedRigidBodyForce * (slowDownSpeed * Time.deltaTime);
           
                // Apply the deacceleration force
                PredictionRigidbody.AddForce(decelerationForce, ForceMode.VelocityChange);

                // Ensure that if the object stopped or almost stopped - set its velocity to 0,
                // so it won't jitter due to very small velocities.
                if (_rigidbody.velocity.magnitude < 0.01f)
                {
                    PredictionRigidbody.Velocity(Vector3.zero);
                }
            }
        
            void MoveCharacter(Vector2 input)
            {
                Vector3 mappedMoveInput = new Vector3(-input.x, 0, input.y).normalized;
                //Vector3 additionalForce = mappedMoveInput * (moveSpeed * (float)TimeManager.TickDelta);
                Vector3 additionalForce = mappedMoveInput * (moveSpeed * Time.deltaTime);
                PredictionRigidbody.AddForce(additionalForce, ForceMode.VelocityChange);
                ApplyMaxSpeedCap();
            }
        
            void ApplyMaxSpeedCap()
            {
                if (_rigidbody.velocity.magnitude > maxMoveSpeed)
                {
                    PredictionRigidbody.Velocity(_rigidbody.velocity.normalized * maxMoveSpeed);
                }
            }
        }
    
        //return value can be useful, but is not used for normal Update groundCheck
        private bool GroundCheck()
        {
            CapsuleCollider capsuleCollider = GetComponent<CapsuleCollider>();
            Vector3 p1 = transform.position + capsuleCollider.center + Vector3.up * -capsuleCollider.height * 0.5F;
            Vector3 p2 = p1 + Vector3.up * capsuleCollider.height;
            
            RaycastHit[] rays = Physics.CapsuleCastAll(p2, p1, capsuleCollider.radius * groundCheckWidth, Vector3.down, groundDetectionLength, groundLayers, QueryTriggerInteraction.Ignore);
            List<RaycastHit> filteredRays = new List<RaycastHit>();
            foreach (RaycastHit ray in rays)
            {
                if (ray.collider.gameObject != gameObject)
                {
                    filteredRays.Add(ray);
                }
                
            }
            if (filteredRays.Count > 0)
            {
                _isGrounded = true;
                return true;
            }
            _isGrounded = false;
            return false;
        }
    
        public void OnMove(InputAction.CallbackContext context)
        {
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            if (!IsOwner) return;
        
            if(context.performed) _jumpTriggered = true;
        }

        public void OnMouseLook(InputAction.CallbackContext context)
        {
            if (!IsOwner) return;
        
            Vector2 mouseDelta = context.ReadValue<Vector2>();
            Vector3 newTransformRotation = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + (mouseDelta.x * turnRate), transform.localEulerAngles.z);
            transform.eulerAngles = newTransformRotation;
            Debug.Log("Rotation: " + (GameManager.MainCamera.transform.localEulerAngles.x + (-mouseDelta.y * turnRate))%360);
            float newXRotation = (GameManager.MainCamera.transform.localEulerAngles.x + (-mouseDelta.y * turnRate))%360;
            if (newXRotation > lookUpMax && newXRotation < lookDownMax)
            {
                if (newXRotation < 180)
                {
                    newXRotation = lookDownMax;
                }
                else
                {
                    newXRotation = lookUpMax;
                }
            }
            Vector3 newCameraRotation = new Vector3(newXRotation, GameManager.MainCamera.transform.localEulerAngles.y, GameManager.MainCamera.transform.localEulerAngles.z);
            GameManager.MainCamera.transform.localEulerAngles = newCameraRotation;
        }

        private void OnDestroy()
        {
            ObjectCaches<PredictionRigidbody>.StoreAndDefault(ref PredictionRigidbody);
        }
    }
}
