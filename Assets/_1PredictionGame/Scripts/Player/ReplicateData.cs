﻿using FishNet.Object.Prediction;
using UnityEngine;

namespace _1PredictionGame.Scripts.Player
{
    public struct ReplicateData : IReplicateData
    {
        public bool Jump;
        public Vector2 MoveVector;
        
        public ReplicateData(bool jump, Vector2 moveVector)
        {
            Jump = jump;
            MoveVector = moveVector;
            _tick = 0;
        }

        private uint _tick;
        public void Dispose() { }
        public uint GetTick() => _tick;
        public void SetTick(uint value) => _tick = value;
    }
    
    public struct ReconcileData : IReconcileData
    {
        //PredictionRigidbody is used to synchronize rigidbody states
        //and forces. This could be done manually but the PredictionRigidbody
        //type makes this process considerably easier. Velocities, kinematic state,
        //transform properties, pending velocities and more are automatically
        //handled with PredictionRigidbody.
        public PredictionRigidbody PredictionRigidbody;
    
        public ReconcileData(PredictionRigidbody pr) : this()
        {
            PredictionRigidbody = pr;
        }

        private uint _tick;
        public void Dispose() { }
        public uint GetTick() => _tick;
        public void SetTick(uint value) => _tick = value;
    }
}