using _1PredictionGame.Scripts.Control;
using FishNet.Connection;
using FishNet.Object;
using UnityEngine;

namespace _1PredictionGame.Scripts.Player
{
    public class PlayerControl : NetworkBehaviour
    {
        public override void OnStartClient()
        {
            if (IsOwner)
            {
                GameManager.MainCamera.transform.SetParent(transform);
                GameManager.MainCamera.transform.localPosition = Vector3.zero;
                GameManager.MainCamera.transform.localRotation = Quaternion.identity;
            }
        }
    }
}
